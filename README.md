# Webmagic

#### 介绍
基于WebMagic的网络爬虫,实现了CSDN的登录操作

#### 软件架构
基于webmagic框架的爬虫,webmagic是什么就不用多说了


#### 安装教程

下载后将项目转成maven项目就可以了

#### 使用说明

1. crawlerCsdnLogin.java类展示了使用Selenium来渲染页面模拟用户在页面进行操作

2. LoginDemo.java类展示了使用jsoup来爬取静态网页，对于那种可以在页面源码中看到登录信息的网页，可以直接使用这种方式登录

3. GithubRepoPageProcessor.java这个类就展示了使用jsoup静态登录获取cookie再结合webmagic登录网站，爬取需要的信息，纯粹是为了练习展示用

