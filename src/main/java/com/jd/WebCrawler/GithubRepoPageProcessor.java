package com.jd.WebCrawler;

import java.util.Map;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.Spider;
import us.codecraft.webmagic.pipeline.JsonFilePipeline;
import us.codecraft.webmagic.processor.PageProcessor;

public class GithubRepoPageProcessor implements PageProcessor {
	// 抓取网站的相关配置，重试次数，休眠时间，超时时间，设置循环次数（如果失败就重新放入到队列中,直到达到充实次数）
	private static Site site = Site.me().setRetryTimes(3).setSleepTime(0).setTimeOut(3000).setCycleRetryTimes(3);
	private static int count = 0;

	public Site getSite() {
		return site;
	}

	public void process(Page page) {

		// 加入满足条件的链接
		page.addTargetRequests(page.getHtml().xpath("//*span[@class='number']/span/a/@href").all());

		count++;
		
		//爬取保存到page中													可以将不需要的内容替换掉
		page.putField("data", page.getHtml().xpath("//*td/tidyText()").replace("\n", "").replace("<>", "").all());
		
	}
	
	public static void main(String[] args) {
		Long str;
		str = System.currentTimeMillis();
		
		//创建LogDemo对象，调用login方法，来获取cookie
		LoginDemo log = new LoginDemo();
		Map<String, String> login = null;

		try {
			//传入用户名和密码
			login = log.login("username", "password");
		} catch (Exception e) {
			e.printStackTrace();
		}

		//将获取的cookies设置到site中
		for (String s : login.keySet()) {
			site.addCookie(s, login.get(s));
		}

		//传入一个网页入口，初始化线程池中的线程数，开启爬虫
		Spider.create(new GithubRepoPageProcessor())
				.addPipeline(new JsonFilePipeline("D:\\webmagic\\"))
				.addUrl("*********************")//这里输入你要爬取的url
				.thread(5)
				.run();
		
		System.out.println("爬取了" + count + "条内容,用时：" + ((System.currentTimeMillis()-str)/1000 ));
	}

}
