package com.jd.WebCrawler;

import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import us.codecraft.webmagic.Page;
import us.codecraft.webmagic.Site;
import us.codecraft.webmagic.processor.PageProcessor;

public class crawlerCsdnLogin implements PageProcessor {
	
	private static Site site = Site.me().setRetryTimes(3).setSleepTime(100);
	
	//保存登录过后的cookies
	private static Set<Cookie> cookies;

	@Override
	public void process(Page page) {
		System.out.println(page.getHtml().toString());
	}

	@Override
	public Site getSite() {
		return site;
	}

	public static void main(String[] args) throws FileNotFoundException {
		
		System.setProperty("webdriver.chrome.driver",
                "C:\\Users\\JD_Zang\\AppData\\Local\\Google\\Chrome\\Application\\chromedriver.exe");
        WebDriver driver = new ChromeDriver();
        driver.get("https://passport.csdn.net/login");
        
        //找到"账号登录"按钮
        List<WebElement> findElements = driver.findElement(By.className("main-select")).findElements(By.tagName("a"));

        //点击页面中的"账号登录"按钮
        findElements.get(1).click();
        
        //填入用户面馆和密码
        driver.findElement(By.id("all")).sendKeys("username");
        driver.findElement(By.id("password-number")).sendKeys("password");
        
        //找到页面中的"登录"按钮
		List<WebElement> findElements2 = driver.findElements(By.className("form-group").xpath("//div/button"));
        
        //点击"登录"按钮，提交表单
        findElements2.get(0).click();
        
        //将cookies保存起来，后续就不需要再进行登录的操作了
        cookies = driver.manage().getCookies();
        
        //将获取的cookies保存到site中
        Iterator<Cookie> iterator = cookies.iterator();
        while (iterator.hasNext()) {
        	Cookie next = iterator.next();
        	site.addCookie(next.getName(), next.getValue());
		}
        
        //完成后将资源关闭
//        driver.close();
        
	}
}
